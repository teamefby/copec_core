from copec_core.variables import *
import boto3
import json
import datetime

class Utils(object) :

    def __init__(self):
        aws_access_key_id = AWSAccessKeyId
        aws_secret_access_key = AWSSecretKey
        self.region_name = AWSRegionName

        ################################################      
        # CONFIGURAMOS LOS ENDPOINT SQS Y SNS
        ################################################
        self.sns_client = boto3.client('sns',
            aws_access_key_id = aws_access_key_id, 
            aws_secret_access_key = aws_secret_access_key, 
            region_name = self.region_name)

        self.sqs_client = boto3.client('sqs',
            aws_access_key_id = aws_access_key_id, 
            aws_secret_access_key = aws_secret_access_key, 
            region_name = self.region_name)
        

        self.sqs_url = SQS_URL
        self.topic_arn_sns = TOPIC_ARN_SNS

    def send_message_sqs(self, path, request, retries):
    
        ########################################################
        # CONFIGURAMOS EL MENSAJE PARA LA COLA SQS
        ########################################################
        data = {
            "path": path, 
            "request": request,
            "retries": str(retries)
        }

        ########################################################
        #Body message
        ########################################################
        print('sqs:data {}'.format(json.dumps(data)))
        
        ########################################################
        # ENVIAMOS MENSAJE A LA COLA SQS
        ########################################################
        response = self.sqs_client.send_message(
            QueueUrl = self.sqs_url,
            MessageAttributes = {},
        MessageBody = json.dumps(data))
        print('sqs:send_message {}'.format(response['MessageId']))

    def receive_message_sqs(self, receipt_handle):
        #######################################################
        # RECIBIMOS MENSAJE DE LA COLA SQS
        #######################################################
        messages = self.sqs_client.receive_message(
            QueueUrl = self.sqs_url,
            AttributeNames = ['SentTimestamp'],
            MaxNumberOfMessages = 1,
            MessageAttributeNames = ['All'],
            VisibilityTimeout = 0,
            WaitTimeSeconds = 5)
        print('sqs:receive_message {}'.format(messages))


        ########################################################    
        # ELIMINAMOS MENSAJE DESDE SQS
        ########################################################
        try:
            if 'Messages' in messages:
                messages = messages['Messages'][0]
                receipt_handle = messages['ReceiptHandle']
                self.sqs_client.delete_message(
                    QueueUrl = self.sqs_url,
                    ReceiptHandle = receipt_handle)
                print('sqs:delete_message {}'.format(messages))
        except KeyError:
            print('sqs:empty {}'.format(datetime.datetime.now()))
            messages = []
    
    def delete_message_sqs(self, receipt_handle, message_id):
        self.sqs_client.delete_message(
            QueueUrl = self.sqs_url,
            ReceiptHandle = receipt_handle)
        print('sqs:delete_message message_id {}'.format(message_id))
        print('sqs:delete_message receipt_handle {}'.format(receipt_handle))

    def send_message_sns(self, subject):
        now = datetime.datetime.now()
        self.sns_client.publish(
          TopicArn = self.topic_arn_sns,
          Message = "ERROR EN LA CARGA DE LOS DATOS {}".format(now),
          Subject = subject)
        print('sns:publish {}'.format(now))
    
    def get_secret(self, key):
        try:
            secret_name = key
            session = boto3.session.Session()
            client = session.client(
                service_name = 'secretsmanager',
                region_name = self.region_name
            )
            
            get_secret_value_response = client.get_secret_value(
                SecretId=secret_name
            )

            if 'SecretString' in get_secret_value_response:
                return get_secret_value_response['SecretString']
            else:
                return base64.b64decode(get_secret_value_response['SecretBinary'])
        except:
            print("Something went wrong secrets keys: {}".format('get_secret'))
            return "Fail"