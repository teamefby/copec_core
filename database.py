import mysql.connector
import json
import copec_core.variables
from copec_core.utils import Utils  

class DataBase(object):

    def __init__(self):
        utils = Utils()

        ########################################################     
        # OBTENEMOS EL SECRETO DESDE SECRETSMANAGER
        ########################################################
        secreto = json.loads(utils.get_secret("PROSALE_SAP"))
        print('secrets: {}'.format(secreto))
        ########################################################     
        # CONFIG A LA BASE DE DATOS
        ########################################################
        user_db = secreto["dbuser_qa"]
        pass_db = secreto["dbpassword_qa"]
        host_db = copec_core.variables.HOST_DATABASE
        name_db = copec_core.variables.NAME_DATABASE

        self.conn = mysql.connector.connect(
            user = user_db, 
            password = pass_db,
            host = host_db, 
            database = name_db, 
            autocommit = False
        )
        self.cursor = None

    def print_result(self):
        for result in self.cursor.stored_results():
            jOutput = result.fetchall()
        print('stored_results: {}'.format(jOutput))

    def connect(self):
        self.cursor = self.conn.cursor()
        print('mysql: {}'.format('connected'))

    def close(self):
        if(self.conn.is_connected()):
            self.cursor.close()
            self.conn.close()
            print('mysql: {}'.format('connection is closed'))

    def save(self, procedure, row):
        self.cursor.callproc(procedure, list(row.values()))
    
    def rollback(self):
        self.conn.rollback()
    
    def commit(self):
        self.conn.commit()
    
    def parse_path_to_store_procedure(self, path):
        prefijo = 'PA_CU'
        proc_store = prefijo + path.replace("/", "_").upper()[0:-1]
        return proc_store

    def is_connect(self):
        if self.conn.is_connected():
            return True
        return False
